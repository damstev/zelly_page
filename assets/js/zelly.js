/////////////////////////Instalación cookie primer visita ZELLY//////////////
function setCookie(cname,cvalue,exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires=" + d.toGMTString();
	document.cookie = cname+"="+cvalue+"; "+expires;
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1);
		if (c.indexOf(name) != -1) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function checkCookie() {
	var uservisit=getCookie("firstvisit");
	if (uservisit != "") {
		//No es la primer visita//
	} else {
		//Es la primer vez//
		mixpanel.track('page viewed', {
		'page name' : document.title,
		'url' : window.location.pathname,
		'firstvisit': 'yes'
		});

		uservisit = "visited";
		setCookie("firstvisit", uservisit, 365);
	}
}
/////////////////////////////Fin Instalacion coockie primer visita ADIKTIVO//////////////////////

<!-- start Mixpanel -->
(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
mixpanel.init("583d7922c88ac90a83d97e6c982b9225", {'loaded':function() {
        checkCookie()
    }
    });
<!-- end Mixpanel -->


	console.log('Zelly.js running');

	////////////Start Survey/////////////////
	$(document).off("click").on("click",".btn.btn-warning.btn-lg",function(){
		mixpanel.track("Started",{"Buttom":"Vender ahora"});
	});

	var content, status, humidity, imei, charger, information, imei_number, first_name, last_name, email, phone;


	
	$(document).off("mouseover").on("mouseover",".btn.btn-success",function(){
		var url=window.location.href;

		console.log('mouseover');

		if (url=='http://app.zelly.co/#/step1') {
			content=$('.ng-binding.ng-scope').text();
		}
		
		if (url=='http://app.zelly.co/#/step4') {
			status=$('input:radio[name=status]:checked').val()
			humidity=$('input:radio[name=humidity]:checked').val()
			imei=$('input:radio[name=imei]:checked').val()
			charger=$('input:radio[name=charger]:checked').val()
			information=$('input:radio[name=information]:checked').val()
			imei_number=$('[name="imei_number"]').val();
		}


		if (url=='http://app.zelly.co/#/step5') {
			first_name=$('#first_name').val();
			last_name=$('#last_name').val();
			email=$('#email').val();
			phone=$('#phone').val();
			content=$('.ng-binding.ng-scope').text();
		}


		$(document).off("click").on("click",".btn.btn-success",function(){	
			console.log('click url:', url);

			///////////Track Step 1////////////////
			if (url=='http://app.zelly.co/#/step1') {
				mixpanel.track("Step1", {'#step': url,'Brand-Model': content});
				mixpanel.people.set({"u-Brand-Model":content});
			}
			///////////Track Step 2////////////////
			if (url=='http://app.zelly.co/#/step2') {
				mixpanel.track("Step2", {'#step': url,'content': 'Agreed Conditions'});
			}
			///////////Track Step 3////////////////
			if (url=='http://app.zelly.co/#/step3') {
				mixpanel.track("Step3", {'#step': url,'content': 'Uploaded Photos'});
			}
			///////////Track Step 4////////////////
			if (url=='http://app.zelly.co/#/step4') {
				mixpanel.track("Step4", {'#step': url,'content': 'Device Info', 'status':status, 'humidity':humidity, 'imei':imei, 'charger':charger, 'information':information, 'imei_number':imei_number});
				mixpanel.people.set({'u-status':status, 'u-humidity':humidity, 'u-imei':imei, 'u-charger':charger, 'u-information':information, 'u-imei_number':imei_number});
			}
			///////////Track Step 5////////////////
			if (url=='http://app.zelly.co/#/step5') {
				mixpanel.alias(email);
				mixpanel.people.set({
				    "$email": email,
				    "$first_name": first_name,
				    "$last_name": last_name,
				    "$phone": phone,
				    "$created": new Date(),
				    "u-City":content
				});
				mixpanel.track("Step5", {'#step': url,'City': content});
				mixpanel.track("Registered", {'#step': url,'City': content});
			}

		});
	});

